add_library(mbed-drivers 
    mbed-os/drivers/AnalogIn.h
    mbed-os/drivers/AnalogOut.h
    mbed-os/drivers/BufferedSerial.h
    mbed-os/drivers/BusIn.h
    mbed-os/drivers/BusInOut.h
    mbed-os/drivers/BusOut.h
    mbed-os/drivers/CAN.h
    mbed-os/drivers/DigitalIn.h
    mbed-os/drivers/DigitalInOut.h
    mbed-os/drivers/DigitalOut.h
    mbed-os/drivers/FlashIAP.h
    mbed-os/drivers/I2C.h
    mbed-os/drivers/I2CSlave.h
    mbed-os/drivers/InterruptIn.h
    mbed-os/drivers/LowPowerTicker.h
    mbed-os/drivers/LowPowerTimeout.h
    mbed-os/drivers/LowPowerTimer.h
    mbed-os/drivers/MbedCRC.h
    mbed-os/drivers/PortIn.h
    mbed-os/drivers/PortInOut.h
    mbed-os/drivers/PortOut.h
    mbed-os/drivers/PwmOut.h
    mbed-os/drivers/QSPI.h
    mbed-os/drivers/ResetReason.h
    mbed-os/drivers/SPI.h
    mbed-os/drivers/SPISlave.h
    mbed-os/drivers/SerialBase.h
    mbed-os/drivers/SerialWireOutput.h
    mbed-os/drivers/Ticker.h
    mbed-os/drivers/Timeout.h
    mbed-os/drivers/Timer.h
    mbed-os/drivers/TimerEvent.h
    # mbed-os/drivers/USBAudio.h
    # mbed-os/drivers/USBCDC.h
    # mbed-os/drivers/USBCDC_ECM.h
    # mbed-os/drivers/USBHID.h
    # mbed-os/drivers/USBKeyboard.h
    # mbed-os/drivers/USBMIDI.h
    # mbed-os/drivers/USBMSD.h
    # mbed-os/drivers/USBMouse.h
    # mbed-os/drivers/USBMouseKeyboard.h
    # mbed-os/drivers/USBSerial.h
    mbed-os/drivers/UnbufferedSerial.h
    mbed-os/drivers/Watchdog.h
    mbed-os/drivers/internal/AsyncOp.h
    mbed-os/drivers/internal/ByteBuffer.h
    mbed-os/drivers/internal/EndpointResolver.h
    mbed-os/drivers/internal/LinkEntry.h
    mbed-os/drivers/internal/LinkedList.h
    mbed-os/drivers/internal/LinkedListBase.h
    mbed-os/drivers/internal/MIDIMessage.h
    mbed-os/drivers/internal/OperationList.h
    mbed-os/drivers/internal/OperationListBase.h
    mbed-os/drivers/internal/PolledQueue.h
    mbed-os/drivers/internal/SFDP.h
    mbed-os/drivers/internal/Task.h
    mbed-os/drivers/internal/TaskBase.h
    mbed-os/drivers/internal/TaskQueue.h
    # mbed-os/drivers/internal/USBAudio_Types.h
    # mbed-os/drivers/internal/USBDescriptor.h
    # mbed-os/drivers/internal/USBDevice.h
    # mbed-os/drivers/internal/USBDevice_Types.h
    # mbed-os/drivers/internal/USBHID_Types.h
    mbed-os/drivers/source/AnalogIn.cpp
    mbed-os/drivers/source/AnalogOut.cpp
    mbed-os/drivers/source/BufferedSerial.cpp
    mbed-os/drivers/source/BusIn.cpp
    mbed-os/drivers/source/BusInOut.cpp
    mbed-os/drivers/source/BusOut.cpp
    mbed-os/drivers/source/CAN.cpp
    mbed-os/drivers/source/DigitalIn.cpp
    mbed-os/drivers/source/DigitalInOut.cpp
    mbed-os/drivers/source/DigitalOut.cpp
    mbed-os/drivers/source/FlashIAP.cpp
    mbed-os/drivers/source/I2C.cpp
    mbed-os/drivers/source/I2CSlave.cpp
    mbed-os/drivers/source/InterruptIn.cpp
    mbed-os/drivers/source/MbedCRC.cpp
    mbed-os/drivers/source/PortIn.cpp
    mbed-os/drivers/source/PortInOut.cpp
    mbed-os/drivers/source/PortOut.cpp
    mbed-os/drivers/source/PwmOut.cpp
    mbed-os/drivers/source/QSPI.cpp
    mbed-os/drivers/source/ResetReason.cpp
    mbed-os/drivers/source/SFDP.cpp
    mbed-os/drivers/source/SPI.cpp
    mbed-os/drivers/source/SPISlave.cpp
    mbed-os/drivers/source/SerialBase.cpp
    mbed-os/drivers/source/SerialWireOutput.cpp
    mbed-os/drivers/source/Ticker.cpp
    mbed-os/drivers/source/Timeout.cpp
    mbed-os/drivers/source/Timer.cpp
    mbed-os/drivers/source/TimerEvent.cpp
    mbed-os/drivers/source/UnbufferedSerial.cpp
    mbed-os/drivers/source/Watchdog.cpp
    # mbed-os/drivers/source/usb/AsyncOp.cpp
    # mbed-os/drivers/source/usb/ByteBuffer.cpp
    # mbed-os/drivers/source/usb/EndpointResolver.cpp
    # mbed-os/drivers/source/usb/LinkedListBase.cpp
    # mbed-os/drivers/source/usb/OperationListBase.cpp
    # mbed-os/drivers/source/usb/PolledQueue.cpp
    # mbed-os/drivers/source/usb/TaskBase.cpp
    # mbed-os/drivers/source/usb/USBAudio.cpp
    # mbed-os/drivers/source/usb/USBCDC.cpp
    # mbed-os/drivers/source/usb/USBCDC_ECM.cpp
    # mbed-os/drivers/source/usb/USBDevice.cpp
    # mbed-os/drivers/source/usb/USBHID.cpp
    # mbed-os/drivers/source/usb/USBKeyboard.cpp
    # mbed-os/drivers/source/usb/USBMIDI.cpp
    # mbed-os/drivers/source/usb/USBMSD.cpp
    # mbed-os/drivers/source/usb/USBMouse.cpp
    # mbed-os/drivers/source/usb/USBMouseKeyboard.cpp
    # mbed-os/drivers/source/usb/USBSerial.cpp
)

target_include_directories(mbed-drivers PUBLIC
    mbed-os/drivers/internal
    mbed-os/drivers
)

target_link_libraries(mbed-drivers PUBLIC mbed-platform mbed-rtos)