add_library(mbed-client-libservice
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/common_functions.h
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/ip4string.h
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/ip6string.h
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/ip_fsc.h
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/ns_list.h
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/ns_nvm_helper.h
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/ns_trace.h
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/ns_types.h
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/nsdynmemLIB.h
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/platform/arm_hal_interrupt.h
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/platform/arm_hal_nvm.h
)

target_include_directories(mbed-client-libservice PUBLIC
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice/platform
    mbed-os/features/frameworks/nanostack-libservice/mbed-client-libservice
    mbed-os/features/frameworks/nanostack-libservice
)

target_link_libraries(mbed-client-libservice PUBLIC mbed-os mbed-trace)