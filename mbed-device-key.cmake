add_library(mbed-device-key
    mbed-os/features/device_key/source/DeviceKey.cpp
    mbed-os/features/device_key/source/DeviceKey.h
)

target_include_directories(mbed-device-key PUBLIC
    mbed-os/features/device_key/source
    mbed-os/features/device_key
)

target_link_libraries(mbed-device-key PUBLIC mbed-os)