add_library(mbed-client-cli
    mbed-os/features/frameworks/mbed-client-cli/mbed-client-cli
    mbed-os/features/frameworks/mbed-client-cli
)

target_include_directories(mbed-client-cli PUBLIC
    mbed-os/features/frameworks/mbed-client-cli/mbed-client-cli/ns_cmdline.h
    mbed-os/features/frameworks/mbed-client-cli/source/ns_cmdline.c
)

target_link_libraries(mbed-client-cli PUBLIC mbed-os)