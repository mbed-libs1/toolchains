add_library(mbed-mesh-api
    mbed-os/features/nanostack/mbed-mesh-api/mbed-mesh-api/LoWPANNDInterface.h
    mbed-os/features/nanostack/mbed-mesh-api/mbed-mesh-api/MeshInterfaceNanostack.h
    mbed-os/features/nanostack/mbed-mesh-api/mbed-mesh-api/NanostackEthernetInterface.h
    mbed-os/features/nanostack/mbed-mesh-api/mbed-mesh-api/NanostackPPPInterface.h
    mbed-os/features/nanostack/mbed-mesh-api/mbed-mesh-api/ThreadInterface.h
    mbed-os/features/nanostack/mbed-mesh-api/mbed-mesh-api/WisunInterface.h
    mbed-os/features/nanostack/mbed-mesh-api/mbed-mesh-api/mesh_interface_types.h
    mbed-os/features/nanostack/mbed-mesh-api/source/CallbackHandler.cpp
    mbed-os/features/nanostack/mbed-mesh-api/source/LoWPANNDInterface.cpp
    mbed-os/features/nanostack/mbed-mesh-api/source/MeshInterfaceNanostack.cpp
    mbed-os/features/nanostack/mbed-mesh-api/source/NanostackEMACInterface.cpp
    mbed-os/features/nanostack/mbed-mesh-api/source/NanostackEthernetInterface.cpp
    mbed-os/features/nanostack/mbed-mesh-api/source/NanostackMemoryManager.cpp
    mbed-os/features/nanostack/mbed-mesh-api/source/NanostackPPPInterface.cpp
    mbed-os/features/nanostack/mbed-mesh-api/source/ThreadInterface.cpp
    mbed-os/features/nanostack/mbed-mesh-api/source/WisunInterface.cpp
    mbed-os/features/nanostack/mbed-mesh-api/source/ethernet_tasklet.c
    mbed-os/features/nanostack/mbed-mesh-api/source/include/NanostackMemoryManager.h
    mbed-os/features/nanostack/mbed-mesh-api/source/include/callback_handler.h
    mbed-os/features/nanostack/mbed-mesh-api/source/include/enet_tasklet.h
    mbed-os/features/nanostack/mbed-mesh-api/source/include/mesh_system.h
    mbed-os/features/nanostack/mbed-mesh-api/source/include/nd_tasklet.h
    mbed-os/features/nanostack/mbed-mesh-api/source/include/thread_tasklet.h
    mbed-os/features/nanostack/mbed-mesh-api/source/include/wisun_tasklet.h
    mbed-os/features/nanostack/mbed-mesh-api/source/mesh_system.c
    mbed-os/features/nanostack/mbed-mesh-api/source/nd_tasklet.c
    mbed-os/features/nanostack/mbed-mesh-api/source/thread_tasklet.c
    mbed-os/features/nanostack/mbed-mesh-api/source/wisun_tasklet.c
)

target_include_directories(mbed-mesh-api PUBLIC
    mbed-os/features/nanostack/mbed-mesh-api/source/include
    mbed-os/features/nanostack/mbed-mesh-api/source
    mbed-os/features/nanostack/mbed-mesh-api/mbed-mesh-api
    mbed-os/features/nanostack/mbed-mesh-api
)

target_link_libraries(mbed-mesh-api PUBLIC mbed-os mbed-netsocket mbed-sal-stack-nanostack mbed-nanostack-interface mbed-nanostack-hal-cmsis-rtos mbed-client-libservice)