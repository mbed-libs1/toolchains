add_library(mbed-client-randlib
    mbed-os/features/frameworks/mbed-client-randlib/mbed-client-randlib/platform/arm_hal_random.h
    mbed-os/features/frameworks/mbed-client-randlib/mbed-client-randlib/randLIB.h
    mbed-os/features/frameworks/mbed-client-randlib/source/randLIB.c
)

target_include_directories(mbed-client-randlib PUBLIC
    mbed-os/features/frameworks/mbed-client-randlib/mbed-client-randlib/platform
    mbed-os/features/frameworks/mbed-client-randlib/mbed-client-randlib
    mbed-os/features/frameworks/mbed-client-randlib
)

target_link_libraries(mbed-client-randlib PUBLIC mbed-os)