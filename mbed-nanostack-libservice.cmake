add_library(mbed-nanostack-libservice
    mbed-os/features/frameworks/nanostack-libservice/source/IPv6_fcf_lib/ip_fsc.c
    mbed-os/features/frameworks/nanostack-libservice/source/libBits/common_functions.c
    mbed-os/features/frameworks/nanostack-libservice/source/libList/ns_list.c
    mbed-os/features/frameworks/nanostack-libservice/source/libip4string/ip4tos.c
    mbed-os/features/frameworks/nanostack-libservice/source/libip4string/stoip4.c
    mbed-os/features/frameworks/nanostack-libservice/source/libip6string/ip6tos.c
    mbed-os/features/frameworks/nanostack-libservice/source/libip6string/stoip6.c
    mbed-os/features/frameworks/nanostack-libservice/source/nsdynmemLIB/nsdynmemLIB.c
    mbed-os/features/frameworks/nanostack-libservice/source/nvmHelper/ns_nvm_helper.c
)

target_include_directories(mbed-nanostack-libservice PUBLIC
    mbed-os/features/frameworks/nanostack-libservice
)

target_link_libraries(mbed-nanostack-libservice PUBLIC mbed-os mbed-client-libservice)