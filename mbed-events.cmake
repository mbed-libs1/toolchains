add_library(mbed-events
    mbed-os/events/Event.h
    mbed-os/events/EventQueue.h
    mbed-os/events/UserAllocatedEvent.h
    mbed-os/events/equeue.h
    mbed-os/events/internal/equeue_platform.h
    mbed-os/events/mbed_events.h
    mbed-os/events/mbed_shared_queues.h
    mbed-os/events/source/EventQueue.cpp
    mbed-os/events/source/equeue.c
    mbed-os/events/source/equeue_mbed.cpp
    mbed-os/events/source/equeue_posix.c
    mbed-os/events/source/mbed_shared_queues.cpp
)
target_include_directories(mbed-events PUBLIC 
    mbed-os/events/internal
    mbed-os/events
)
target_link_libraries(mbed-events PUBLIC mbed-os)