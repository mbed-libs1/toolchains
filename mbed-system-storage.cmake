add_library(mbed-system-storage
    mbed-os/features/storage/system_storage/SystemStorage.cpp
)

target_link_libraries(mbed-system-storage PUBLIC mbed-os)