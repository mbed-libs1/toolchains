add_library(mbed-kvstore
    mbed-os/features/storage/kvstore/conf/kv_config.cpp
    mbed-os/features/storage/kvstore/conf/kv_config.h
    mbed-os/features/storage/kvstore/direct_access_devicekey/DirectAccessDevicekey.cpp
    mbed-os/features/storage/kvstore/direct_access_devicekey/DirectAccessDevicekey.h
    mbed-os/features/storage/kvstore/filesystemstore/FileSystemStore.cpp
    mbed-os/features/storage/kvstore/filesystemstore/FileSystemStore.h
    mbed-os/features/storage/kvstore/global_api/kvstore_global_api.cpp
    mbed-os/features/storage/kvstore/global_api/kvstore_global_api.h
    mbed-os/features/storage/kvstore/include/KVStore.h
    mbed-os/features/storage/kvstore/kv_map/KVMap.cpp
    mbed-os/features/storage/kvstore/kv_map/KVMap.h
    mbed-os/features/storage/kvstore/securestore/SecureStore.cpp
    mbed-os/features/storage/kvstore/securestore/SecureStore.h
    mbed-os/features/storage/kvstore/tdbstore/TDBStore.cpp
    mbed-os/features/storage/kvstore/tdbstore/TDBStore.h
)

target_include_directories(mbed-kvstore PUBLIC
    mbed-os/features/storage/kvstore/tdbstore
    mbed-os/features/storage/kvstore/securestore
    mbed-os/features/storage/kvstore/kv_map
    mbed-os/features/storage/kvstore/include
    mbed-os/features/storage/kvstore/global_api
    mbed-os/features/storage/kvstore/filesystemstore
    mbed-os/features/storage/kvstore/direct_access_devicekey
    mbed-os/features/storage/kvstore/conf
    mbed-os/features/storage/kvstore
)

target_link_libraries(mbed-kvstore PUBLIC mbed-os mbed-trace mbed-tls)