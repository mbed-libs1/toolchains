add_library(mbed-wifi
    mbed-os/components/wifi/esp8266-driver/ESP8266/ESP8266.cpp
    mbed-os/components/wifi/esp8266-driver/ESP8266/ESP8266.h
    mbed-os/components/wifi/esp8266-driver/ESP8266Interface.cpp
    mbed-os/components/wifi/esp8266-driver/ESP8266Interface.h
)

target_include_directories(mbed-wifi PUBLIC
    mbed-os/components/wifi/esp8266-driver/ESP8266
    mbed-os/components/wifi/esp8266-driver
    mbed-os/components/wifi
)

target_link_libraries(mbed-wifi PUBLIC mbed-os mbed-netsocket)