add_library(mbed-blockdevice
    mbed-os/features/storage/blockdevice/BlockDevice.h
    mbed-os/features/storage/blockdevice/BufferedBlockDevice.cpp
    mbed-os/features/storage/blockdevice/BufferedBlockDevice.h
    mbed-os/features/storage/blockdevice/ChainingBlockDevice.cpp
    mbed-os/features/storage/blockdevice/ChainingBlockDevice.h
    mbed-os/features/storage/blockdevice/ExhaustibleBlockDevice.cpp
    mbed-os/features/storage/blockdevice/ExhaustibleBlockDevice.h
    mbed-os/features/storage/blockdevice/FlashSimBlockDevice.cpp
    mbed-os/features/storage/blockdevice/FlashSimBlockDevice.h
    mbed-os/features/storage/blockdevice/HeapBlockDevice.cpp
    mbed-os/features/storage/blockdevice/HeapBlockDevice.h
    mbed-os/features/storage/blockdevice/MBRBlockDevice.cpp
    mbed-os/features/storage/blockdevice/MBRBlockDevice.h
    mbed-os/features/storage/blockdevice/ObservingBlockDevice.cpp
    mbed-os/features/storage/blockdevice/ObservingBlockDevice.h
    mbed-os/features/storage/blockdevice/ProfilingBlockDevice.cpp
    mbed-os/features/storage/blockdevice/ProfilingBlockDevice.h
    mbed-os/features/storage/blockdevice/ReadOnlyBlockDevice.cpp
    mbed-os/features/storage/blockdevice/ReadOnlyBlockDevice.h
    mbed-os/features/storage/blockdevice/SlicingBlockDevice.cpp
    mbed-os/features/storage/blockdevice/SlicingBlockDevice.h
)

target_include_directories(mbed-blockdevice PUBLIC 
    mbed-os/features/storage/blockdevice
)

target_link_libraries(mbed-blockdevice PUBLIC mbed-os)