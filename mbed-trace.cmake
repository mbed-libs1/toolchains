add_library(mbed-trace
    mbed-os/features/frameworks/mbed-trace/mbed-trace/mbed_trace.h
    mbed-os/features/frameworks/mbed-trace/source/mbed_trace.c
)

target_include_directories(mbed-trace PUBLIC
    mbed-os/features/frameworks/mbed-trace/mbed-trace
    mbed-os/features/frameworks/mbed-trace
)

target_link_libraries(mbed-trace PUBLIC mbed-os mbed-client-libservice)