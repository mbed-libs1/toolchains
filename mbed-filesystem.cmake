add_library(mbed-filesystem
    mbed-os/features/storage/filesystem/Dir.cpp
    mbed-os/features/storage/filesystem/Dir.h
    mbed-os/features/storage/filesystem/File.cpp
    mbed-os/features/storage/filesystem/File.h
    mbed-os/features/storage/filesystem/FileSystem.cpp
    mbed-os/features/storage/filesystem/FileSystem.h
    mbed-os/features/storage/filesystem/fat/ChaN/diskio.h
    mbed-os/features/storage/filesystem/fat/ChaN/ff.cpp
    mbed-os/features/storage/filesystem/fat/ChaN/ff.h
    mbed-os/features/storage/filesystem/fat/ChaN/ffconf.h
    mbed-os/features/storage/filesystem/fat/ChaN/ffunicode.cpp
    mbed-os/features/storage/filesystem/fat/ChaN/integer.h
    mbed-os/features/storage/filesystem/fat/FATFileSystem.cpp
    mbed-os/features/storage/filesystem/fat/FATFileSystem.h
    mbed-os/features/storage/filesystem/littlefs/LittleFileSystem.cpp
    mbed-os/features/storage/filesystem/littlefs/LittleFileSystem.h
    mbed-os/features/storage/filesystem/littlefs/littlefs/lfs.c
    mbed-os/features/storage/filesystem/littlefs/littlefs/lfs.h
    mbed-os/features/storage/filesystem/littlefs/littlefs/lfs_util.c
    mbed-os/features/storage/filesystem/littlefs/littlefs/lfs_util.h
    mbed-os/features/storage/filesystem/mbed_filesystem.h
)

target_include_directories(mbed-filesystem PUBLIC
    mbed-os/features/storage/filesystem/littlefs/littlefs
    mbed-os/features/storage/filesystem/littlefs
    mbed-os/features/storage/filesystem/fat/ChaN
    mbed-os/features/storage/filesystem/fat
    mbed-os/features/storage/filesystem
    mbed-os/features/storage
)

target_link_libraries(mbed-filesystem PUBLIC mbed-os)