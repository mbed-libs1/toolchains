add_library(mbed-coap
    mbed-os/features/frameworks/mbed-coap/mbed-coap/sn_coap_header.h
    mbed-os/features/frameworks/mbed-coap/mbed-coap/sn_coap_protocol.h
    mbed-os/features/frameworks/mbed-coap/mbed-coap/sn_config.h
    mbed-os/features/frameworks/mbed-coap/source/include/sn_coap_header_internal.h
    mbed-os/features/frameworks/mbed-coap/source/include/sn_coap_protocol_internal.h
    mbed-os/features/frameworks/mbed-coap/source/sn_coap_builder.c
    mbed-os/features/frameworks/mbed-coap/source/sn_coap_header_check.c
    mbed-os/features/frameworks/mbed-coap/source/sn_coap_parser.c
    mbed-os/features/frameworks/mbed-coap/source/sn_coap_protocol.c
)
target_include_directories(mbed-coap PUBLIC
    mbed-os/features/frameworks/mbed-coap/source/include
    mbed-os/features/frameworks/mbed-coap/source
    mbed-os/features/frameworks/mbed-coap/mbed-coap
    mbed-os/features/frameworks/mbed-coap
)
target_link_libraries(mbed-coap PUBLIC mbed-os mbed-client-libservice mbed-client-randlib)