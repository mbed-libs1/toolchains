add_library(mbed-unity
    mbed-os/features/frameworks/unity/source/unity.c
    mbed-os/features/frameworks/unity/unity/unity.h
    mbed-os/features/frameworks/unity/unity/unity_config.h
    mbed-os/features/frameworks/unity/unity/unity_internals.h
)

target_include_directories(mbed-unity PUBLIC
    mbed-os/features/frameworks/unity/unity
    mbed-os/features/frameworks/unity
)

target_link_libraries(mbed-unity PUBLIC mbed-os mbed-utest)