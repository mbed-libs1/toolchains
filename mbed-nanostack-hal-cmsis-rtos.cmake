add_library(mbed-nanostack-hal-cmsis-rtos
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/arm_hal_fhss_timer.cpp
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/arm_hal_interrupt.c
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/arm_hal_interrupt_private.h
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/arm_hal_random.c
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/arm_hal_timer.cpp
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/ns_event_loop.c
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/ns_event_loop.h
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/ns_event_loop_mbed.cpp
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/ns_event_loop_mutex.c
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/ns_event_loop_mutex.h
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/ns_hal_init.c
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/ns_hal_init.h
    mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos/nvm/nvm_ram.c
)

target_include_directories(mbed-nanostack-hal-cmsis-rtos PUBLIC
  mbed-os/features/nanostack/nanostack-hal-mbed-cmsis-rtos
)

target_link_libraries(mbed-nanostack-hal-cmsis-rtos PUBLIC mbed-os mbed-client-libservice mbed-sal-stack-nanostack-eventloop mbed-client-randlib mbed-tls mbed-sal-stack-nanostack mbed-events)