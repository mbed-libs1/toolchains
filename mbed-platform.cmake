add_library(mbed-platform
    mbed-os/platform/ATCmdParser.h
    mbed-os/platform/CThunk.h
    mbed-os/platform/Callback.h
    mbed-os/platform/CircularBuffer.h
    mbed-os/platform/CriticalSectionLock.h
    mbed-os/platform/DeepSleepLock.h
    mbed-os/platform/DirHandle.h
    mbed-os/platform/FileBase.h
    mbed-os/platform/FileHandle.h
    mbed-os/platform/FileLike.h
    mbed-os/platform/FilePath.h
    mbed-os/platform/FileSystemHandle.h
    mbed-os/platform/FileSystemLike.h
    mbed-os/platform/LocalFileSystem.h
    mbed-os/platform/NonCopyable.h
    mbed-os/platform/PlatformMutex.h
    mbed-os/platform/ScopedLock.h
    mbed-os/platform/ScopedRamExecutionLock.h
    mbed-os/platform/ScopedRomWriteLock.h
    mbed-os/platform/SharedPtr.h
    mbed-os/platform/SingletonPtr.h
    mbed-os/platform/Span.h
    mbed-os/platform/Stream.h
    mbed-os/platform/Transaction.h
    mbed-os/platform/critical.h
    mbed-os/platform/cxxsupport/mstd_algorithm
    mbed-os/platform/cxxsupport/mstd_atomic
    mbed-os/platform/cxxsupport/mstd_cstddef
    mbed-os/platform/cxxsupport/mstd_functional
    mbed-os/platform/cxxsupport/mstd_iterator
    mbed-os/platform/cxxsupport/mstd_memory
    mbed-os/platform/cxxsupport/mstd_mutex
    mbed-os/platform/cxxsupport/mstd_mutex.cpp
    mbed-os/platform/cxxsupport/mstd_new
    mbed-os/platform/cxxsupport/mstd_tuple
    mbed-os/platform/cxxsupport/mstd_type_traits
    mbed-os/platform/cxxsupport/mstd_utility
    mbed-os/platform/internal/CThunkBase.h
    mbed-os/platform/internal/mbed_atomic_impl.h
    mbed-os/platform/mbed_application.h
    mbed-os/platform/mbed_assert.h
    mbed-os/platform/mbed_atomic.h
    mbed-os/platform/mbed_critical.h
    mbed-os/platform/mbed_debug.h
    mbed-os/platform/mbed_error.h
    mbed-os/platform/mbed_interface.h
    mbed-os/platform/mbed_mem_trace.h
    mbed-os/platform/mbed_mktime.h
    mbed-os/platform/mbed_mpu_mgmt.h
    mbed-os/platform/mbed_poll.h
    mbed-os/platform/mbed_power_mgmt.h
    mbed-os/platform/mbed_preprocessor.h
    mbed-os/platform/mbed_retarget.h
    mbed-os/platform/mbed_rtc_time.h
    mbed-os/platform/mbed_semihost_api.h
    mbed-os/platform/mbed_sleep.h
    mbed-os/platform/mbed_stats.h
    mbed-os/platform/mbed_thread.h
    mbed-os/platform/mbed_toolchain.h
    mbed-os/platform/mbed_version.h
    mbed-os/platform/mbed_wait_api.h
    mbed-os/platform/platform.h
    mbed-os/platform/rtc_time.h
    mbed-os/platform/semihost_api.h
    mbed-os/platform/sleep.h
    mbed-os/platform/source/ATCmdParser.cpp
    mbed-os/platform/source/CThunkBase.cpp
    mbed-os/platform/source/CriticalSectionLock.cpp
    mbed-os/platform/source/DeepSleepLock.cpp
    mbed-os/platform/source/FileBase.cpp
    mbed-os/platform/source/FileHandle.cpp
    mbed-os/platform/source/FilePath.cpp
    mbed-os/platform/source/FileSystemHandle.cpp
    mbed-os/platform/source/LocalFileSystem.cpp
    mbed-os/platform/source/Stream.cpp
    mbed-os/platform/source/SysTimer.cpp
    mbed-os/platform/source/SysTimer.h
    mbed-os/platform/source/TARGET_CORTEX_M/TOOLCHAIN_GCC/except.S
    mbed-os/platform/source/TARGET_CORTEX_M/mbed_fault_handler.c
    mbed-os/platform/source/TARGET_CORTEX_M/mbed_fault_handler.h
    mbed-os/platform/source/mbed_alloc_wrappers.cpp
    mbed-os/platform/source/mbed_application.c
    mbed-os/platform/source/mbed_assert.c
    mbed-os/platform/source/mbed_atomic_impl.c
    mbed-os/platform/source/mbed_board.c
    mbed-os/platform/source/mbed_crash_data_offsets.h
    mbed-os/platform/source/mbed_critical.c
    mbed-os/platform/source/mbed_error.c
    mbed-os/platform/source/mbed_error_hist.c
    mbed-os/platform/source/mbed_error_hist.h
    mbed-os/platform/source/mbed_interface.c
    mbed-os/platform/source/mbed_mem_trace.cpp
    mbed-os/platform/source/mbed_mktime.c
    mbed-os/platform/source/mbed_mpu_mgmt.c
    mbed-os/platform/source/mbed_os_timer.cpp
    mbed-os/platform/source/mbed_os_timer.h
    mbed-os/platform/source/mbed_poll.cpp
    mbed-os/platform/source/mbed_power_mgmt.c
    mbed-os/platform/source/mbed_retarget.cpp
    mbed-os/platform/source/mbed_rtc_time.cpp
    mbed-os/platform/source/mbed_sdk_boot.c
    mbed-os/platform/source/mbed_semihost_api.c
    mbed-os/platform/source/mbed_stats.c
    mbed-os/platform/source/mbed_thread.cpp
    mbed-os/platform/source/mbed_wait_api_no_rtos.c
    mbed-os/platform/source/mbed_wait_api_rtos.cpp
    mbed-os/platform/source/minimal-printf/mbed_printf_armlink_overrides.c
    mbed-os/platform/source/minimal-printf/mbed_printf_implementation.c
    mbed-os/platform/source/minimal-printf/mbed_printf_implementation.h
    mbed-os/platform/source/minimal-printf/mbed_printf_wrapper.c
    mbed-os/platform/toolchain.h
    mbed-os/platform/wait_api.h
)

target_include_directories(mbed-platform PUBLIC
    mbed-os/platform/source/minimal-printf
    mbed-os/platform/source/TARGET_CORTEX_M
    mbed-os/platform/source
    mbed-os/platform/internal
    mbed-os/platform/cxxsupport
    mbed-os/platform
    mbed-os
)

target_link_libraries(mbed-platform PUBLIC mbed-cmsis mbed-targets mbed-rtos)