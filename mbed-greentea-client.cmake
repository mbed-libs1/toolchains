add_library(mbed-greentea-client
    mbed-os/features/frameworks/greentea-client/greentea-client/greentea_metrics.h
    mbed-os/features/frameworks/greentea-client/greentea-client/test_env.h
    mbed-os/features/frameworks/greentea-client/source/greentea_metrics.cpp
    mbed-os/features/frameworks/greentea-client/source/greentea_test_env.cpp
)

target_include_directories(mbed-greentea-client PUBLIC
    mbed-os/features/frameworks/greentea-client/greentea-client
    mbed-os/features/frameworks/greentea-client
    mbed-os/features/frameworks
)

target_link_libraries(mbed-greentea-client PUBLIC mbed-os mbed-trace)