add_library(mbed-utest
    mbed-os/features/frameworks/utest/mbed-utest-shim.cpp
    mbed-os/features/frameworks/utest/source/unity_handler.cpp
    mbed-os/features/frameworks/utest/source/utest_case.cpp
    mbed-os/features/frameworks/utest/source/utest_default_handlers.cpp
    mbed-os/features/frameworks/utest/source/utest_greentea_handlers.cpp
    mbed-os/features/frameworks/utest/source/utest_harness.cpp
    mbed-os/features/frameworks/utest/source/utest_print.cpp
    mbed-os/features/frameworks/utest/source/utest_shim.cpp
    mbed-os/features/frameworks/utest/source/utest_stack_trace.cpp
    mbed-os/features/frameworks/utest/source/utest_types.cpp
    mbed-os/features/frameworks/utest/utest/unity_handler.h
    mbed-os/features/frameworks/utest/utest/utest.h
    mbed-os/features/frameworks/utest/utest/utest_case.h
    mbed-os/features/frameworks/utest/utest/utest_default_handlers.h
    mbed-os/features/frameworks/utest/utest/utest_harness.h
    mbed-os/features/frameworks/utest/utest/utest_print.h
    mbed-os/features/frameworks/utest/utest/utest_scheduler.h
    mbed-os/features/frameworks/utest/utest/utest_shim.h
    mbed-os/features/frameworks/utest/utest/utest_specification.h
    mbed-os/features/frameworks/utest/utest/utest_stack_trace.h
    mbed-os/features/frameworks/utest/utest/utest_types.h
)

target_include_directories(mbed-utest PUBLIC
    mbed-os/features/frameworks/utest/utest
    mbed-os/features/frameworks/utest
    mbed-os/features/frameworks
)

target_link_libraries(mbed-utest PUBLIC mbed-os mbed-greentea-client)