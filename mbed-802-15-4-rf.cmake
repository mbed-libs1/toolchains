add_library(mbed-802-15-4-rf
    mbed-os/components/802.15.4_RF/atmel-rf-driver/atmel-rf-driver/NanostackRfPhyAtmel.h
    mbed-os/components/802.15.4_RF/atmel-rf-driver/source/AT86RFReg.h
    mbed-os/components/802.15.4_RF/atmel-rf-driver/source/NanostackRfPhyAtmel.cpp
    mbed-os/components/802.15.4_RF/atmel-rf-driver/source/at24mac.cpp
    mbed-os/components/802.15.4_RF/atmel-rf-driver/source/at24mac.h
    mbed-os/components/802.15.4_RF/mcr20a-rf-driver/mcr20a-rf-driver/NanostackRfPhyMcr20a.h
    mbed-os/components/802.15.4_RF/mcr20a-rf-driver/source/MCR20Drv.c
    mbed-os/components/802.15.4_RF/mcr20a-rf-driver/source/MCR20Drv.h
    mbed-os/components/802.15.4_RF/mcr20a-rf-driver/source/MCR20Overwrites.h
    mbed-os/components/802.15.4_RF/mcr20a-rf-driver/source/MCR20Reg.h
    mbed-os/components/802.15.4_RF/mcr20a-rf-driver/source/NanostackRfPhyMcr20a.cpp
    mbed-os/components/802.15.4_RF/mcr20a-rf-driver/source/XcvrSpi.h
    mbed-os/components/802.15.4_RF/stm-s2lp-rf-driver/source/NanostackRfPhys2lp.cpp
    mbed-os/components/802.15.4_RF/stm-s2lp-rf-driver/source/at24mac_s2lp.cpp
    mbed-os/components/802.15.4_RF/stm-s2lp-rf-driver/source/at24mac_s2lp.h
    mbed-os/components/802.15.4_RF/stm-s2lp-rf-driver/source/rf_configuration.c
    mbed-os/components/802.15.4_RF/stm-s2lp-rf-driver/source/rf_configuration.h
    mbed-os/components/802.15.4_RF/stm-s2lp-rf-driver/source/s2lpReg.h
    mbed-os/components/802.15.4_RF/stm-s2lp-rf-driver/stm-s2lp-rf-driver/NanostackRfPhys2lp.h
)

target_include_directories(mbed-802-15-4-rf PUBLIC
    mbed-os/components/802.15.4_RF/stm-s2lp-rf-driver/stm-s2lp-rf-driver
    mbed-os/components/802.15.4_RF/stm-s2lp-rf-driver/source
    mbed-os/components/802.15.4_RF/stm-s2lp-rf-driver
    mbed-os/components/802.15.4_RF/mcr20a-rf-driver/source
    mbed-os/components/802.15.4_RF/mcr20a-rf-driver/mcr20a-rf-driver
    mbed-os/components/802.15.4_RF/mcr20a-rf-driver
    mbed-os/components/802.15.4_RF/atmel-rf-driver/source
    mbed-os/components/802.15.4_RF/atmel-rf-driver/atmel-rf-driver
    mbed-os/components/802.15.4_RF/atmel-rf-driver
    mbed-os/components/802.15.4_RF
)

target_link_libraries(mbed-802-15-4-rf PUBLIC mbed-os mbed-nanostack-interface)