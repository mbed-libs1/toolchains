add_library(mbed-nanostack-interface
    mbed-os/features/nanostack/nanostack-interface/Nanostack.cpp
    mbed-os/features/nanostack/nanostack-interface/Nanostack.h
    mbed-os/features/nanostack/nanostack-interface/NanostackEthernetPhy.h
    mbed-os/features/nanostack/nanostack-interface/NanostackInterface.h
    mbed-os/features/nanostack/nanostack-interface/NanostackLockGuard.h
    mbed-os/features/nanostack/nanostack-interface/NanostackMACPhy.h
    mbed-os/features/nanostack/nanostack-interface/NanostackPPPPhy.h
    mbed-os/features/nanostack/nanostack-interface/NanostackPhy.h
    mbed-os/features/nanostack/nanostack-interface/NanostackRfPhy.h
)

target_include_directories(mbed-nanostack-interface PUBLIC
    mbed-os/features/nanostack/nanostack-interface
)

target_link_libraries(mbed-nanostack-interface PUBLIC mbed-os mbed-netsocket mbed-mesh-api)