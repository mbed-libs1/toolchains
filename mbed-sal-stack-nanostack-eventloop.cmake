add_library(mbed-sal-stack-nanostack-eventloop
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/nanostack-event-loop/eventOS_callback_timer.h
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/nanostack-event-loop/eventOS_event.h
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/nanostack-event-loop/eventOS_event_timer.h
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/nanostack-event-loop/eventOS_scheduler.h
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/nanostack-event-loop/platform/arm_hal_timer.h
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/nanostack-event-loop/platform/eventloop_config.h
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/source/event.c
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/source/event.h
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/source/ns_timeout.c
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/source/ns_timer.c
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/source/ns_timer.h
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/source/system_timer.c
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/source/timer_sys.h
)

target_include_directories(mbed-sal-stack-nanostack-eventloop PUBLIC
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/nanostack-event-loop
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop/source
    mbed-os/features/nanostack/sal-stack-nanostack-eventloop
)

target_link_libraries(mbed-sal-stack-nanostack-eventloop PUBLIC mbed-os mbed-client-libservice)