add_library(mbed-coap-service
    mbed-os/features/nanostack/coap-service/coap-service/coap_service_api.h
    mbed-os/features/nanostack/coap-service/source/coap_connection_handler.c
    mbed-os/features/nanostack/coap-service/source/coap_message_handler.c
    mbed-os/features/nanostack/coap-service/source/coap_security_handler.c
    mbed-os/features/nanostack/coap-service/source/coap_service_api.c
    mbed-os/features/nanostack/coap-service/source/include/coap_connection_handler.h
    mbed-os/features/nanostack/coap-service/source/include/coap_message_handler.h
    mbed-os/features/nanostack/coap-service/source/include/coap_security_handler.h
    mbed-os/features/nanostack/coap-service/source/include/coap_service_api_internal.h
)

target_include_directories(mbed-coap-service PUBLIC
    mbed-os/features/nanostack/coap-service/source/include
    mbed-os/features/nanostack/coap-service/source
    mbed-os/features/nanostack/coap-service/coap-service
    mbed-os/features/nanostack/coap-service
)

target_link_libraries(mbed-coap-service PUBLIC mbed-os mbed-client-libservice mbed-tls mbed-coap mbed-sal-stack-nanostack)